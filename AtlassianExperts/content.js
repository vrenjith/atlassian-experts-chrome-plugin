function debug(message){
    //console.log(message);
}

$(document).ready(function () {
    loadExpertData();
});

function isDataEmpty(){
    var expertsData = JSON.parse(localStorage.getItem('expertData'));
    if(null != expertsData && Object.keys(expertsData).length > 0) {
        return false;
    } else {
        return true;
    }
}

function loadExpertData() {
    var prevTime = JSON.parse(localStorage.getItem('lastexpertchecktime'));
    debug("Previous load time " + prevTime)
    //If the last load time was more than a day
    if (null == prevTime || (new Date().getTime() - prevTime > 86400000) || isDataEmpty() ) {
        debug("Loading new data");
        var expertsData = {};
        var expertsDomain = {};
        $.get('https://extranet.atlassian.com/pages/viewpage.action?pageId=2126884222', function (data) {
            debug("Expert page access complete");
            $(data).find('.cviz-chart meta').each(function () {
                debug("Found chart data");
                var abc = $(this).attr('content');
                var expArray = $.parseJSON($.parseJSON($($.parseJSON(abc)).attr('wizard-step-select')).lastData).data.numbers;
                for (var cnt = 0; cnt < expArray.length; cnt++) {
                    expertsData[expArray[cnt][0]] = expArray[cnt];
		    if( null == expertsDomain[expArray[cnt][1]] ) {//Take just the first one, there can be incomplete experts without complete data
                    	expertsDomain[expArray[cnt][1]] = expArray[cnt];
		    }                }
                debug("Expert data loaded for one block");
            });
            localStorage.setItem('expertData', JSON.stringify(expertsData));
            localStorage.setItem('expertsDomain', JSON.stringify(expertsDomain));
            localStorage.setItem('lastexpertchecktime', JSON.stringify(new Date().getTime()));
            updateReporter(expertsData, expertsDomain);
        });
    } else {
        debug('Skipping expert data load, not yet 24 hours');
        var expertsData = JSON.parse(localStorage.getItem('expertData'));
        var expertsDomain = JSON.parse(localStorage.getItem('expertsDomain'));
        updateReporter(expertsData, expertsDomain);
    }
}

function updateReporter(expertsData, expertsDomain) {
	var userName = $('#reporter-val .user-hover').attr("rel");
    if(null != userName && "" != userName) {
        $.getJSON('https://support.atlassian.com/rest/api/latest/user?username=' + $('#reporter-val .user-hover').attr("rel"), function (data) {
            var reporterEmail = data.emailAddress;

            if (null != reporterEmail) {

                var thisExpert = expertsData[reporterEmail];
                if (null == thisExpert) {
                    var thisDomain = reporterEmail.split('@')[1];
                    var ignoreDomains = ['gmail','yahoo', 'hotmail', 'outlook'];
                    var freeEmail = false;
                    for (var i = 0; i < ignoreDomains.length; i++) {
                        var idom = ignoreDomains[i];
                        debug("Checking for - " + idom);
                        if(thisDomain.indexOf(idom) != -1) {
                            debug("Free email domain - " + idom);
                            freeEmail = true;
                            break;
                        }
                    }
                    if(!freeEmail) {
                        thisExpert = expertsDomain[thisDomain];
                        if (null == thisExpert) {
                            debug('Not an expert');
                        }
                        else {
                            debug('Expert matched with domain');
                        }
                    } else {
                        debug("Free email domain, ignoring");
                    }
                }
                else {
                    debug('Expert matched with email');
                }

                if (null != thisExpert) {
                    debug(thisExpert);
                    var messageType = "info";
                    var iconType = "icon-info"
                    if('Inactive' == thisExpert[2]) {
                        messageType = "warning";
                        iconType = "icon-warning";
                    }
                    $('#reporter-val .user-hover').parent().parent().parent().after('<div class="aui-message expertdatadiv ' + messageType + ' shadowed"> \
        <p class="title"> <span class="aui-icon ' + iconType + '"></span> <strong>Atlassian Expert</strong></p></div>');
                    $('.expertdatadiv').append('<table border="0" cellpadding="1" cellspacing = "1" style = "width: 100%;" > <tbody> <tr> <td> Name</td> <td>'
                            + thisExpert[6] + '</td> </tr> <tr> <td> Country</td> <td> '
                            + thisExpert[5] + '</td> </tr> </tr> <tr> <td> Status</td> <td> '
                            + thisExpert[2] + ' </td> </tr> </tbody> </table>'
                    );
                }
            }
        });
    }
}
